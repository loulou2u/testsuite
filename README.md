Testsuite

This is a work in progress and will hold a collection of basic selenium webdriver 
test cases and examples. 

The following project(s) have been performed using the following technologies:

1. Java 1.8
2. Eclipse IDE 4.6 (Neon)
3. Maven
4. Selenium version 3.0.0.beta-2

The browser versions I tested were:
- Chrome 51.0.2704.103
- Firefox 48.0

Remember to download the browser drivers and save them to a location (ex., /usr/local/bin)

* Firefox driver: https://github.com/mozilla/geckodriver/releases

* Chrome driver: https://sites.google.com/a/chromium.org/chromedriver/

* Safari driver: https://github.com/SeleniumHQ/selenium/wiki/SafariDriver



