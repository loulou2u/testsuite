package testsuite;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class GoogleTest {
	private static WebDriver driver; 
	String appURL = "http://google.com";

	@BeforeClass
	public static void setUp() {
		//setup the driver
		System.setProperty("webdriver.gecko.driver","/usr/local/bin/geckodriver");
		driver = new FirefoxDriver();
	}
	
	@Test
	public void openGooglePage() {
		//Open Google in Firefox browser
		driver.navigate().to(appURL);
	}
	
	@AfterClass
	public static void tearDown() {
		//Close out the browser
		driver.quit();
	}
}
